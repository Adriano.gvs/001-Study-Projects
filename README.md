# Meu Projetos de Estudos

Neste repositorio contem diversos cursos dos quais estou fazendo, sendo:
<b />
<b />
Item | Curso | Carga Horária 
---|---|---
01 | MBA em Tecnologia para Negócios AI, Data Science e Big Data. | 660 Horas
02 | Data Science em Produção | 452 Horas
03 | Bootcamp Desenvolver Python | 148 Horas
04 | Microsoft Power BI Para Data Science, Versão 2.0 | 72 Horas
05 | Python Fundamentos Para Análise de Dados 3.0 | 60 Horas
