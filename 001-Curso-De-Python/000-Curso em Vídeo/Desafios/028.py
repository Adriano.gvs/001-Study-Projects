"""
Escreva um programa que faça o computador "pensar" em um numero de 0 a 5 e peça para o usuário tentar descobror qual foi o número
escolhido pelo computador.

O programa deverá escrever na tela se o usuário venceu ou perdeu.
"""


import random


num = int(input('Qual numero o computador escolheu de 1 a 5? '))
lista = [1, 2, 3, 4, 5]
escolhido = random.choice(lista)


if escolhido == num:
    print('Você Venceu')
else:
    print('Você Perdeu')